import {DependencyService, getDependency, registerDependency} from './dependency-service';

describe('Dependency Service', () => {
	describe('Direct Use', () => {
		let dependencyService: DependencyService;

		beforeEach(() => {
			dependencyService = DependencyService.getInstance();
		});

		it('lets us get a singleton instance', () => {
			expect(dependencyService).toBeTruthy();
		});

		it('lets us register and get a value', () => {
			dependencyService.register('value', 'one');
			expect(dependencyService.get('value')).toEqual('one');
		});

		it('lets us register and get another value', () => {
			dependencyService.register('key1', 'value1');
			expect(dependencyService.get('key1')).toEqual('value1');
		});
	});

	describe('Global Functions', () => {
		it('lets us avoid calling getInstance()', () => {
			registerDependency('key99', 'value99');
			expect(getDependency('key99')).toEqual('value99');
		});
	});
});
