export class DependencyService {
	static INSTANCE: DependencyService = new DependencyService();

	static getInstance() {
		return this.INSTANCE;
	}

	private dependencies: Map<string, any>;

	private constructor() {
		this.dependencies = new Map<string, any>();
	}

	register(key: string, dependency: any) {
		this.dependencies.set(key, dependency);
	}

	get(key: string) {
		return this.dependencies.get(key);
	}
}

export const registerDependency = (key: string, dependency: any) => {
	DependencyService.getInstance().register(key, dependency);
};

export const getDependency: any = (key: string) => {
	return DependencyService.getInstance().get(key);
};
